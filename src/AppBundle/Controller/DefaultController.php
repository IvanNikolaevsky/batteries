<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Batteries;
use AppBundle\Form\BatteriesForm;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $batteries = $this->getDoctrine()
            ->getRepository('AppBundle:Batteries')->findAllGroupedByType();

        return $this->render('AppBundle:Default:index.html.twig', array('batteries' => $batteries));
    }

    /**
     * @Route("/add", name="add_battery")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(new BatteriesForm());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('AppBundle:Default:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
