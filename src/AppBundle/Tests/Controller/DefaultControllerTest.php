<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    private $submitButton = 'Add battery';
    public function testFormSubmit()
    {
        $this->clearTable();

        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $crawler = $client->click($crawler->selectLink('here')->link());

        $form = $crawler->selectButton($this->submitButton)->form(array(
            'batteries[count]'  => '4',
            'batteries[type]'  => 'AAA',
            'batteries[name]'  => 'Test'
        ));
        $client->submit($form);
        $form = $crawler->selectButton($this->submitButton)->form(array(
            'batteries[count]'  => '3',
            'batteries[type]'  => 'AA',
            'batteries[name]'  => 'Test2'
        ));
        $client->submit($form);
        $form = $crawler->selectButton($this->submitButton)->form(array(
            'batteries[count]'  => '2',
            'batteries[type]'  => 'AAA',
            'batteries[name]'  => ''
        ));
        $client->submit($form);
        $crawler = $client->followRedirect();
        $expectedValues = array(
            'AA' => 3,
            'AAA' => 6,
        );
        $crawlerClient = $this;
        $crawler->filter('table > tbody > tr')->each(function($node, $i) use($expectedValues, $crawlerClient) {
            $type  = $node->children()->eq(0)->text();
            $count = $node->children()->eq(1)->text();
            $crawlerClient->assertEquals($expectedValues[$type], $count);
        });
    }

    private function clearTable()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $query = $em->createQuery('DELETE from AppBundle:Batteries b');
        $query->getResult();
    }
}
